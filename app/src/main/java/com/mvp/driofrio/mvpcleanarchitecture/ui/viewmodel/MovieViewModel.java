package com.mvp.driofrio.mvpcleanarchitecture.ui.viewmodel;

/**
 * Todoo lo que va dentro de viewModel es el modelo de la vista (normalmente es igual que el modelo)
 * pero si hay que cambiar algo del modelo por necesidad de la vista se cambia en este modelo porque
 * no pasa nada.
 *
 *
 * @author Antonio López.
 */
public class MovieViewModel {

    private String title;
    private String descripcion;

    public MovieViewModel(String tittle, String descripcion) {
        this.title = tittle;
        this.descripcion = descripcion;
    }

    public MovieViewModel() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * TODO: por ejemplo aqui cambio el toString.
     * @return
     */
    @Override
    public String toString() {
        return title;
    }
}
