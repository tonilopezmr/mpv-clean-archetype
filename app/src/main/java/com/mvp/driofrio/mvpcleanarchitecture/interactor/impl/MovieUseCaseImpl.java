package com.mvp.driofrio.mvpcleanarchitecture.interactor.impl;

import android.widget.Toast;

import com.mvp.driofrio.mvpcleanarchitecture.interactor.MovieUseCase;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.Executor;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.MainThread;
import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;




import javax.security.auth.callback.Callback;

/**
 * Mira aqui no es dificil hay que hacer lo siguiente.
 *
 * Aqui tendrias que tener el repository cuando lo crees y el executor y el MainThread
 *
 * TODO: Importante que llames al MovieUseCase de forma que sepas lo que hace este UseCase por ejemplo MovieGetListUseCase
 *
 * Created by driofrio on 28/05/2015.
 */
public class MovieUseCaseImpl implements MovieUseCase {

    //Por constructor
    private Executor executor;
    private MainThread mainThread;

    //Lo que necesitas al hacer una instruccion (o sea lo del metodo execute)
    private Movie movie;
    private Callback callback;

    public MovieUseCaseImpl(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
    }

    @Override
    public void execute(Movie movie, Callback callback) {

        this.callback = callback;
        this.movie = movie;

        this.executor.run(this);
    }

    /**
     * TODO: IMPORTANTE TIENES QUE USAR EL MAINTHREAD QUE ES LO QUE TE FALTABA
     */
    @Override
    public void run() {

        try {
            Thread.sleep(3000);
        } catch (final InterruptedException e) {
           this.mainThread.post(new Runnable() {
               @Override
               public void run() {
                   callback.onError(e);
               }
           });
        }

        /**
         * ESTO ES PORQUE TU EN ESTE METODO RUN NO ESTAS EN EL HILO DEL MAIN (HILO PRINCIPAL)
         * Y CUANDO QUIERES COMUNICARTE CON EL HILO PRINCIPAL LO HACES CON EL MAINTHREAD!!!!
         *
         * Sino no te funciona.
         */
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onMissionAccomplished(movie);
            }
        });
    }
}
