package com.mvp.driofrio.mvpcleanarchitecture.interactor.executor;

/**
 * @author toni.
 */
public interface Executor {
    void run(final Interactor interactor);
}
