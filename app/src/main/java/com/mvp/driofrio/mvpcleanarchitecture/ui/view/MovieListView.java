package com.mvp.driofrio.mvpcleanarchitecture.ui.view;

import com.mvp.driofrio.mvpcleanarchitecture.ui.viewmodel.MovieViewModel;

import java.util.List;

/**
 * TODO: CAMBIO DE CustomView a MovieListView (CustomView es por ejemplo un boton personalizado)
 *
 * Todoo lo que va dentro de View son los metodos que deberia tener cualquier Activity
 * para poder tener una lista de peliculas.
 *
 * Osea que la actividad o fragment que muestre una lista de peliculas tiene que implementar esta
 * interfaz.
 *
 * @author Antonio López.
 */
public interface MovieListView extends View{
    void showMovies(List<MovieViewModel> movieViewModelList);
    void showError(String error);
    void showMessage(String message);
}
