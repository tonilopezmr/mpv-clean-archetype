package com.mvp.driofrio.mvpcleanarchitecture.ui.view;

/**
 * Esta interfaz es necesaria para distinguir las diferentes interfaces de la vista que se pueden
 * crear, por ejemplo MovieListView o si quieres crear MovieDetail.
 *
 * TODO: (Mira la etiqueta @see que es donde se usa)
 *
 * @author Antonio López.
 * @see com.mvp.driofrio.mvpcleanarchitecture.presenter.Presenter // <---- miralo ahi donde se usa
 */
public interface View {
}
