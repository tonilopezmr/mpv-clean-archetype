package com.mvp.driofrio.mvpcleanarchitecture.presenter;

import com.mvp.driofrio.mvpcleanarchitecture.ui.view.View;

/**
 * TODO: Muy importante al crear un Presenter tener el setView porque no es recomendable
 * TODO: pasarle las vistas por constructor porque da problemas con el Dragger cuando lo pongas.
 * TODO: antes de usar un presenter luego en el activity hacerle el setView !! :D
 *
 * Created by driofrio on 27/05/2015.
 */
public interface Presenter<T extends View> {

    void onInit();

    //TODO: muy importante esto
    void setView(T view);
}
