package com.mvp.driofrio.mvpcleanarchitecture;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;
import com.mvp.driofrio.mvpcleanarchitecture.ui.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by driofrio on 26/05/2015.
 */
public class MyReciclerAdapter extends RecyclerView.Adapter<MyReciclerAdapter.MovieViewHolder> implements View.OnClickListener, View.OnLongClickListener  {

    private List<Movie> movieList;
    private int itemLayout;
    //
    private OnRecyclerViewItemClickListener clickListener;
    private OnRecyclerViewItemLongClickListener longClickListener;

    public MyReciclerAdapter(int itemLayout) {
        this.movieList = new ArrayList<>();
        this.itemLayout = itemLayout;
    }

    public MyReciclerAdapter(List<Movie> movieList, int itemLayout) {
        this.movieList = movieList;
        this.itemLayout = itemLayout;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout, viewGroup, false);
        view.setOnClickListener(this); // IMPORTANTISIMO QUE NO SE TE OLVIDE !!
        view.setOnLongClickListener(this); // IMPORTANTISIMO QUE NO SE TE OLVIDE !!
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder movieViewHolder, int i) {
        Movie itemMovie = movieList.get(i);
        movieViewHolder.getTextView().setText(itemMovie.toString());
        movieViewHolder.itemView.setTag(itemMovie); //TODO: EL setTag hay que hacerselo al itemView del Holder(ya viene al extender del RecyclerViewHolder)
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }

    public int getItemLayout() {
        return itemLayout;
    }

    public void setItemLayout(int itemLayout) {
        this.itemLayout = itemLayout;
    }

    @Override
    public void onClick(View v) {
        if (this.clickListener != null){
            Movie movieTag = (Movie) v.getTag();
            clickListener.onItemClick(v,movieTag);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (longClickListener != null){
            Movie movieTag = (Movie) v.getTag();
            longClickListener.onItemLongClick(v, movieTag);
        }
        return false;
    }

    public void addAll(List<MovieViewModel> movies){
        //this.movieList = movies; //TODO: Cuando cambies la pelicula del modelo Movie por ViewModel
        notifyDataSetChanged(); //Refresh
    }

    public interface OnRecyclerViewItemLongClickListener<Model> {
        public void onItemLongClick(View view, Model subject);  //XD TEN CUIDADO CON EL COPY PASTE
    }

    public interface OnRecyclerViewItemClickListener<Model> {
        public void onItemClick(View view, Model subject); //NO ES Model subject sino Model movie! xDD
    }

    public OnRecyclerViewItemClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(OnRecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public OnRecyclerViewItemLongClickListener getLongClickListener() {
        return longClickListener;
    }

    public void setLongClickListener(OnRecyclerViewItemLongClickListener longClickListener) {
        this.longClickListener = longClickListener;
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder{

        private TextView textView;

        public MovieViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.movie_tittle);
        }

        public TextView getTextView() {
            return textView;
        }
    }
}
