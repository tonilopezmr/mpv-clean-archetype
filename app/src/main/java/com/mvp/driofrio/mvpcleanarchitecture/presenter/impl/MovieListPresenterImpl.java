package com.mvp.driofrio.mvpcleanarchitecture.presenter.impl;

import com.mvp.driofrio.mvpcleanarchitecture.interactor.MovieUseCase;
import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;
import com.mvp.driofrio.mvpcleanarchitecture.presenter.MovieListPresenter;
import com.mvp.driofrio.mvpcleanarchitecture.ui.view.MovieListView;

/**
 * Created by driofrio on 27/05/2015.
 */
public class MovieListPresenterImpl implements MovieListPresenter {



    private MovieUseCase movieUseCase;
    private MovieListView customView; //TODO: Cambiale el nombre de customView

    public MovieListPresenterImpl(MovieUseCase movieUseCase) {
        this.movieUseCase = movieUseCase;
    }

    /**
     * Inicias el presenter que segun tu app deberia mostrar las pelis y decirle a la vista
     * que tiene que hacer, por ejemplo como es una lista de pelis, muestras un progress
     * y cuando tengas las pelis quitas el progress y las muestras.
     *
     */
    @Override
    public void onInit() {
        //Aqui es donde deberias llamar al movieListUseCase el que recoge las pelis y hacer
        // un movieListView.showMovies(List<MovieViewModel..)
    }

    @Override
    public void setView(MovieListView view) {
        this.customView = view;
    }


    @Override
    public void onClickItem(Movie movie) {
        movieUseCase.execute(movie, new MovieUseCase.Callback() {
            @Override
            public void onMissionAccomplished(Movie movie) {
                customView.showMessage(movie.toString());
            }

            @Override
            public void onError(Exception ex) {
                customView.showError(ex.getMessage());  //TODO: Muy importante que si es un error llames a un metodo llamado showError
            }
        });
    }

    @Override
    public void onLongClickItem(Movie movie) {
        movieUseCase.execute(movie, new MovieUseCase.Callback() {
            @Override
            public void onMissionAccomplished(Movie movie) {
                customView.showMessage(movie.toString());
            }

            @Override
            public void onError(Exception ex) {
                customView.showError(ex.getMessage());
            }
        });
    }
}
