package com.mvp.driofrio.mvpcleanarchitecture.presenter;

import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;
import com.mvp.driofrio.mvpcleanarchitecture.ui.view.MovieListView;

/**
 *
 * Created by driofrio on 25/05/2015.
 */
public interface MovieListPresenter extends Presenter<MovieListView>{

    void onClickItem(Movie movie);
    void onLongClickItem(Movie movie);
}
