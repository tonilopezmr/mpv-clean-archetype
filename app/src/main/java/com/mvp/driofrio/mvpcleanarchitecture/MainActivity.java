package com.mvp.driofrio.mvpcleanarchitecture;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mvp.driofrio.mvpcleanarchitecture.interactor.MovieUseCase;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.Executor;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.MainThread;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.MainThreadImp;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.ThreadExecutor;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.impl.MovieUseCaseImpl;
import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;
import com.mvp.driofrio.mvpcleanarchitecture.presenter.MovieListPresenter;
import com.mvp.driofrio.mvpcleanarchitecture.presenter.impl.MovieListPresenterImpl;
import com.mvp.driofrio.mvpcleanarchitecture.ui.view.MovieListView;
import com.mvp.driofrio.mvpcleanarchitecture.ui.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity  implements MovieListView {

    private RecyclerView recyclerView;
    private MovieListPresenter movieListPresenter;

    private MyReciclerAdapter myReciclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Primero necesitas el MainThread y el Executor
        Executor executor = new ThreadExecutor();
        MainThread mainThread = new MainThreadImp();

        //Ahora los UseCases
        final MovieUseCase movieUseCase = new MovieUseCaseImpl(executor, mainThread);

        //Presenter
        movieListPresenter = new MovieListPresenterImpl(movieUseCase);
        movieListPresenter.setView(this); //TODO: IMPORTANTISIMOOO

        recyclerView = (RecyclerView) this.findViewById(R.id.my_recycler_view);

        recyclerView.setHasFixedSize(true);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        List<Movie> listMovies = new ArrayList<>();
        listMovies.add(new Movie("Movie 1", "Descripcion movie 1"));
        listMovies.add(new Movie("Movie 2", "Descripcion movie 2"));
        listMovies.add(new Movie("Movie 3", "Descripcion movie 3"));
        listMovies.add(new Movie("Movie 4", "Descripcion movie 4"));
        listMovies.add(new Movie("Movie 5", "Descripcion movie 5"));
        listMovies.add(new Movie("Movie 6", "Descripcion movie 6"));
        listMovies.add(new Movie("Movie 7", "Descripcion movie 7"));
        listMovies.add(new Movie("Movie 8", "Descripcion movie 8"));
        listMovies.add(new Movie("Movie 9", "Descripcion movie 9"));
        listMovies.add(new Movie("Movie 10", "Descripcion movie 10"));
        listMovies.add(new Movie("Movie 11", "Descripcion movie 11"));
        listMovies.add(new Movie("Movie 12", "Descripcion movie 12"));

        myReciclerAdapter = new MyReciclerAdapter(listMovies,R.layout.movie_tittle);


        /**
         * QUE NO SE TE OLVIDE PONERLE EN EL OnRecyclerViewIntemLongClickListener ponerle que es del modelo Movie
         * que por cierto tendras que cambiar a MovieViewModel.
         */
        myReciclerAdapter.setLongClickListener(new MyReciclerAdapter.OnRecyclerViewItemLongClickListener<Movie>() {
            @Override
            public void onItemLongClick(View view, Movie movie) {
                movieListPresenter.onLongClickItem(movie);
            }
        });

        myReciclerAdapter.setClickListener(new MyReciclerAdapter.OnRecyclerViewItemClickListener<Movie>() {

            @Override
            public void onItemClick(View view, Movie movie) {
                movieListPresenter.onClickItem(movie);
            }
        });

        recyclerView.setAdapter(myReciclerAdapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

/*
    @Override
    public void longClickItem(String mesanje) {
        Toast.makeText(getBaseContext(), mesanje, Toast.LENGTH_LONG);
    }

    @Override
    public void clickItem(String mensaje) {
        Toast.makeText(getBaseContext(), mensaje, Toast.LENGTH_SHORT); //TIO SE TE OLVIDA EL .SHOW jaja eso a mi tambien me pasa
    }
*/
    @Override
    public void showMovies(List<MovieViewModel> movieViewModelList) {
        myReciclerAdapter.addAll(movieViewModelList); //Solo con este metodo mostrara la lista
    }


    @Override
    public void showError(String error) {
        Toast.makeText(getBaseContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
    }
}
