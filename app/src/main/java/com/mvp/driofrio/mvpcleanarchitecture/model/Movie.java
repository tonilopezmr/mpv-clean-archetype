package com.mvp.driofrio.mvpcleanarchitecture.model;

/**
 * Created by driofrio on 25/05/2015.
 */
public class Movie {

    private String title;
    private String descripcion;

    public Movie(String tittle, String descripcion) {
        this.title = tittle;
        this.descripcion = descripcion;
    }

    public Movie() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Movie{ title='" + title + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
